import numpy as np
import logging
from verteil import T_normalverteilung
from em import T_em
from theta import theta

class T_mixture:
    """
    Class representing the actuall mixed distribution

    ...

    Attributes
    ----------
    kern_anz : int
        Number of core distributions
    dim : int
        Dimension of the given distributions and therefore dimension of the mixed distribution itself
    theta_a : theta
        Old parameter set
    theta_n : theta
        New parameter set
    theta_save : theta
        Temporary parameter set
    p_em : T_em
        Reference to the estimated mixture
    konv : int
        0 - Not converged, 1 - converged
    schritt_anz : int
        Number of iteration steps 

    """

    def __init__(self, check, cfg):
        ##logging.info("T_mixture-Konstruktor.\n\tDie Mischverteilung wird aufgebaut")

        self.kern_anz = cfg.kern_anz
        self.dim = cfg.dim
        self.max_step = cfg.max_step
        check[0] = 1

    def generate_em_environment(self, cfg, inputdata, check):
        ##logging.info("T_mixture::generate_em_environment().\n\tDie Mischverteilung wird aus Normalverteilungen erzeugt")

        if check[0] != 1:
            logging.error("Fehler in generate_em_environment")
            return

        phi = [T_normalverteilung() for i in range(self.kern_anz)]
        self.p_em = T_em(cfg, phi, inputdata, check)

    def generate_theta(self, check):
        ##logging.info("T_mixture::generate_theta()\n\tDer Parametervektor wird erzeugt")

        if check[0] == 1:
            theta_length = self.kern_anz * (2 + self.dim)
            self.theta_a = theta(self.kern_anz, self.dim)
            self.theta_n = theta(self.kern_anz, self.dim)
            self.theta_save = theta(self.kern_anz, self.dim)
        else:
            check[0] = 0

    def em_mixture_estimation(self, check):
        ##logging.info("T_mixture::em_mixture_estimation()")
        if check[0] == 1:
            self.generate_theta(check)
        else:
            logging.error("Fehler bei generate_theta()")
            check[0]

        zaehl_non_konv = 0

        seed = 24356 ##TODO add to cfg
        self.p_em.initialize_theta(seed)
        ##self.p_em.initialize_fix(0.5, 0.25, 2.25, 0.5, 1.5) ## 2 components

        ##logging.debug("theta_a = {t}".format(t = self.p_em.theta_a.p + self.p_em.theta_a.sigma + self.p_em.theta_a.mu))

        self.konv = 0
        self.schritt_anz = 0
        loglike = self.em_schritte()

        logging.info("Theta nach Abschluss der Schätzung:\n{t}".format(t = self.p_em.theta_a))

        if self.konv == 0:
            zaehl_non_konv += 1

    def em_schritte(self):
        ##logging.info("T_mixture::em_schritte(2)")

        neu_log = -1.0e+10
        alt_log = 0

        ##logging.info("Maximal {d} Schritte".format(d = self.max_step))

        for i in range(self.max_step):
            self.p_em.calculate_aposteriori()
            self.p_em.maximization_step()
            self.p_em.keep_parameters()

            alt_log = neu_log
            neu_log = self.p_em.loglikelihood()

            ##logging.debug("---\n {d} \n--------".format(d = neu_log))

            if np.abs(alt_log - neu_log) < 1.0e-5:
                print("Konvergenz nach {d} Schritten".format(d = i))
                self.schritt_anz = i
                self.konv = 1
                break

        self.theta_a.override_with(self.p_em.theta_a)

        ##logging.info("Theta: {x}".format(x = self.p_em.theta_a.p + self.p_em.theta_a.sigma + self.p_em.theta_a.mu))
        if self.konv == 0:
            logging.warning("Keine Konvergenz!")
