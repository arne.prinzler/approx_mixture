import numpy as np
import logging

class T_data:
    """
    Class accounting for all datasets, including training-, validation-, and testdatasets.

    ...

    Attributes
    ----------
    count : int
        Number of datasets
    i_matrix : list of float
        Vector of inputdata
    o_matrix : list of float
        Vector of outputdata

    """
    def __init__(self, cfg, check):
        ##logging.info("T_data-Konstruktor.\n\tDas Datenobjekt wird aufgebaut. Hier auch noch Festlegung des Inputfiles")

        self.count = cfg.count

        if check[0] == 1:
            ## schneide chars "x" und "y" ab und entferne die letzte leerzeile
            data = [str.split("\t") for str in open(cfg.input_file, "r").read().split('\n')][1:-1]
            transpose = list(map(list, zip(*data)))
            self.i_matrix = [float(x) for x in transpose[0]]
            self.i_matrix += [0 for i in range(self.count - len(self.i_matrix))] ##set list to length count
            self.o_matrix = [float(x) for x in (transpose[1:])[0]] ##later on only transpose[1:]
            self.o_matrix += [0 for i in range(self.count - len(self.o_matrix))] ##TODO find proper implementation

        else:
            logging.error("Fehler T_data Konstruktor")
