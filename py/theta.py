class theta:
    """
    Container of p, sigma, mu for all distributions

    ...

    Attributes
    ----------
    p : list of float
        List of 'kern_anz' coefficients p
    sigma : list of float
        List of 'kern_anz' coefficients sigma
    mu : list of float
        List where every section of length 'dim' contains the coefficients mu

    """

    def __init__(self, *args):
        if len(args) == 2:
            self.initialize(args[0], args[1])
        elif len(args) == 3:
            self.assign(args[0], args[1], args[2])
        else:
            print("Wrong number of Arguments!")

    def __str__(self):
        return "Theta mit p = {a}\tsigma = {b}\tmu = {c}".format(a = self.p, b = self.sigma, c = self.mu)

    ##maybe not needed 
    def assign(self, p, sigma, mu):
        self.p = p
        self.sigma = sigma
        self.mu = mu

    def initialize(self, kern_anz, dim):
        self.p = [0.0 for i in range(kern_anz)]
        self.sigma = [0.0 for i in range(kern_anz)]
        self.mu = [0.0 for i in range(kern_anz * dim)]

    def override_with(self, theta_n): ##overriding needed!!
        for i in range(len(self.p)):
            self.p[i] = theta_n.p[i]

        for i in range(len(self.sigma)):
            self.sigma[i] = theta_n.sigma[i]

        for i in range(len(self.mu)):
            self.mu[i] = theta_n.mu[i]
