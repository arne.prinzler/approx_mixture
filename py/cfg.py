import logging
class T_cfg:
    """
    Array with associated photographic information.

    ...

    Attributes
    ----------
    kern_anz : int
        Number of distributions.
    inp : int
        Inputdimension
    dim : int
        Outputdimension
    count : int
        Number of datasets.
    max_step : int
        Maximum numbers of interation steps in EM
    """

    def __init__(self, check):
        ##logging.info("T_cfg-Konstruktor\n\tIm Moment wird hier definiert, welche Struktur das Modell hat.\n\tPerspektivisch Definition ueber Konfigurationsfile")
        
        self.kern_anz = 2
        self.inp = 1
        self.dim = 1
        self.count = 1000
        self.max_step = 10000
        self.input_file = "cs.txt"
        check[0] = 1

