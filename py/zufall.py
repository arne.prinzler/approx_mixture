import random
import logging
import numpy as np

class T_zufall:

    def __init__(self, *args):
        if len(args) == 1:
            random.seed(args[0])
        
        self.gauss_gen = self.gasdev()

    def random(self):
        return random.random()

    def gasdev(self):
        ##logging.debug("T_zufall::gasdev()")

        rsq = 0
        condition = True

        while True:
            while condition:
                v1 = 2.0 * random.random() - 1.0
                v2 = 2.0 * random.random() - 1.0
                rsq = v1 * v1 + v2 * v2
                condition = rsq >= 1.0 or rsq == 0.0

            fac = np.sqrt(-2.0 * np.log(rsq) / rsq)
            self.iset = 1

            yield (v2 * fac)
            yield (v1 * fac)

    def generate_rndv(mu, sigma): ##TODO isn't used yet, so I don't know, wether a generator is needed
        return sigma * next(self.gauss_gen) + mu