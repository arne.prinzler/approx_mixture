import logging
import time
from cfg import T_cfg
from data import T_data
from em import T_em
from verteil import T_normalverteilung
from mixture import T_mixture

start_time = time.time()

logfile = open('logfile.log', 'w')
logfile.write('')
logfile.close()
logging.basicConfig(filename='logfile.log', encoding='utf-8', level=logging.DEBUG)

check = [0]
cfg = T_cfg(check)
mixture = T_mixture(check, cfg)
inputdata = T_data(cfg, check)

mixture.generate_em_environment(cfg, inputdata, check)
mixture.em_mixture_estimation(check)

##logging.info("Check: {:d}".format(check[0]))
logging.debug("The program took {d} seconds to execute".format(d = time.time() - start_time))