import numpy as np
import logging

class T_normalverteilung:
    """
    Class for representing normal distributions.

    ...

    Attributes
    ----------
    value : int
        Value of the density of the noraml distribution
    kern_nr : int
        Identification number of the distribution
    dim : int
        Dimension of the distribution
    mischkoeff : list of float
        Vector contains the float of coefficient p at mischkoeff[kern_nr]
    sigma_q : list of float
        Vector contains the float of sigma at sigma_q[kern_nr]
    mu : list of float
        Vector contains the information of mu in the section mu[ker_nr * dim : kern_nr * dim + dim]

    """

    def __init__(self):

        self.value = 0.0
        self.mischkoeff = []
        self.perz = 0.0
        self.mu = []

    def uebergeben(self, dim, kern_nr, kerne, theta):
        self.kern_nr = kern_nr
        self.dim = dim
        self.mischkoeff = theta.p
        self.sigma_q = theta.sigma
        self.mu = theta.mu

    def gauss(self, x):
        ##logging.info("T_normalverteilung::gauss(..)")
        ##logging.debug("arbeitet auf\n\t" + self.toString())
        ##logging.debug("length of x: {a}".format(a = len(x)))
        skalarprodukt = 0.0

        for j in range(self.dim):
            skalarprodukt += (x[j] - self.mu[self.kern_nr*self.dim + j]) ** 2

        self.value = (2*np.pi) ** (-0.5 * self.dim) * self.sigma_q[self.kern_nr] ** (-0.5*self.dim)
        self.value *= np.exp(-skalarprodukt/(2 * (self.sigma_q[self.kern_nr])))

        ##logging.debug("returned value = {res}".format(res = self.value))
        
        return self.value

    ##Debug method
    def toString(self):
        nr = self.kern_nr
        return "Normalverteilung mit p = {a}, sigma = {b}, mu = {c}".format(a = self.mischkoeff[nr], b=self.sigma_q[nr], c=self.mu[nr*self.dim:nr*self.dim+ self.dim])