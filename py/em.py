import numpy as np
import logging
from theta import theta
from verteil import T_normalverteilung
from zufall import T_zufall

class T_em:
    """
    Class for estimating a mixed distribution, given input and output data and a number of distributions

    ...

    Attributes
    ----------
    anzahl : int
        Size of the datasample
    dimension : int
        Dimension of the datavector
    kern_anz : int
        Number of distributions 
    datensatz : list of float
        Data for approximation
    theta_a : theta
        theta of the current approximation
    theta_n : theta
        theta of the newly calculated approximation
    theta_save : theta  ## Porbably not needed
        temporary theta for storing thetas
    kern : list of T_normalverteilung
        Collection of the core distributions
    pi_p : list of float
        Collection of a posteriori probability
    ok : int ## Change to enum or boolean
        0 - invalid state, 1 - everything is fine

    """


    def __init__(self, cfg, komp_den, daten, check):
        ##logging.info("T_EM-Konstruktor.\n\tDie Komponentendichten und die zu modellierenden Daten werden übergeben")
        
        self.anzahl = cfg.count
        self.dimension = cfg.dim
        self.kern_anz = cfg.kern_anz
        self.datensatz = daten.o_matrix ##Will later be matrix

        self.theta_a = theta(self.kern_anz, self.dimension) ##checken ob ich die Initionalisierung brauche
        self.theta_n = []
        self.theta_save = []
        self.kern = komp_den

        for i in range(self.kern_anz):
            self.kern[i].uebergeben(self.dimension, i, self.kern_anz, self.theta_a)

        self.ok = 1
        check[0] = 1

    def __del__(self):
        pass

    ##will not work in non fixed cases
    def initialize_fix(self, *args):
        if len(args) == 5:
            self.initialize_two(args[0], args[1], args[2], args[3], args[4])
        elif len(args) == 8:
            self.initialize_three(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7])
        else:
            logging.error("Falsche Kernzahl!")
            self.ok = 0

    def initialize_two(self, p, var1, var2, mu1, mu2):
        ##logging.info("T_em::initialize_fix für 2 Komponenten")

        self.theta_a.p[0] = p
        self.theta_a.p[1] = 1 - p
        self.theta_a.sigma[0] = var1
        self.theta_a.sigma[1] = var2
        self.theta_a.mu[0] = mu1
        self.theta_a.mu[1] = mu2

    def initialize_three(self, p1, p2, var1, var2, var3, mu1, mu2, mu3):
        ##logging.info("T_em::initialize_fix für 3 Komponenten")

        self.theta_a.p[0] = p1
        self.theta_a.p[1] = p2
        self.theta_a.p[2] = 1.0 - p1 - p2
        self.theta_a.sigma[0] = var1
        self.theta_a.sigma[1] = var2
        self.theta_a.sigma[2] = var3
        self.theta_a.mu[0] = mu1
        self.theta_a.mu[1] = mu2
        self.theta_a.mu[2] = mu3

    def initialize_theta(self, seed):
        rand = T_zufall(seed)

        p = [np.exp(rand.random() * 2.0 - 1.0) for i in range(self.kern_anz)]
        sum_p = sum(p)
        p = [x/sum_p for x in p]
        sigma = [np.exp(rand.random()) for i in range(self.kern_anz)]
        mu = [rand.random() for i in range(self.kern_anz * self.dimension)]

        self.theta_a.override_with(theta(p, sigma, mu))
        ##logging.info("Theta: {x}".format(x = self.theta_a.p + self.theta_a.sigma + self.theta_a.mu))

    def calculate_aposteriori(self):
        ##logging.info("T_em::calculate aposteriori()")

        self.pi_p = []
        for j in range(self.anzahl) :
            sum = 0.0
            for i in range(self.kern_anz) :
                tmp = self.kern[i].gauss(self.datensatz[j * self.dimension :])
                self.pi_p.append(tmp)
                sum += self.theta_a.p[i] * tmp

            for i in range(self.kern_anz) :
                self.pi_p[j*self.kern_anz+i] *= self.theta_a.p[i]/sum
        ##logging.debug("length of pi_p: {a}".format(a = len(self.pi_p)))

    def maximization_step(self):
        ##logging.info("T_em::maximization_step() - Bayes-Version eliminiert")

        sum_lst = [sum(self.pi_p[i: :self.kern_anz]) for i in range(self.kern_anz)]
        new_p = [x/self.anzahl for x in sum_lst]
 
        new_mu = []
        for i in range(self.kern_anz):
            for k in range(self.dimension):
                sum_lst = [self.pi_p[j*self.kern_anz+i] * self.datensatz[j*self.dimension+k] for j in range(self.anzahl)]
                new_mu.append(sum(sum_lst)/(self.anzahl * new_p[i]))

        new_sigma = []
        for i in range(self.kern_anz):
            sum_p = 0.0
            sum_bayes = 0.0
            for j in range(self.anzahl):
                skalarprod = 0.0
                for k in range(self.dimension):
                    skalarprod += (self.datensatz[j*self.dimension+k] - new_mu[i*self.dimension + k]) ** 2
                sum_p += self.pi_p[j*self.kern_anz+i] * skalarprod
                sum_bayes += self.pi_p[j*self.kern_anz+i]

            new_sigma.append(sum_p/(self.dimension*self.anzahl*new_p[i]))

        self.theta_n = theta(new_p, new_sigma, new_mu)

    def keep_parameters(self):
        ##logging.info("T_em::keep_parameters()")

        self.theta_a.override_with(self.theta_n)

    def loglikelihood(self): ##TODO Check loglike
        ##logging.info("T_em::loglikelihood()")

        logl = 0.0
        check = 1

        for kern in self.kern:
            if np.abs(kern.sigma_q[kern.kern_nr]) < 1.0e-5: ##TODO why does it work in his code?
                check = 0

        if check == 1:
            for i in range(self.anzahl):
                sum_p = 0.0
                for j in range(self.kern_anz):
                     sum_p += self.theta_a.p[j] * self.kern[j].gauss(self.datensatz[i*self.dimension :])
                logl += np.log(sum_p)
            return logl
        else:
            return 1.0e+7 ##TODO entferne Magic Number

##Noch nicht fertig
