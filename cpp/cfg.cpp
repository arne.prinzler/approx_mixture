#include "main.h"
//Die Klasse T_cg nimmt alle Parameter auf, mit denen die Mischverteilung und der Schätzalgorithmus konfiguriert werden

//Definitionsbereich
class T_cfg{
	
	public:
	int kern_anz; 					// Anzahl an Normalverteilungskomponenten im Modell
	int in; 						// Inputdimension
	int dim;						// Outputdimension
	int count; 						// Anzahl an Datensätzen
	int max_step;					// der EM-Algorithmus wird maximal max_step mal durchlaufen
	std::ofstream logf;				// zentrales File für logging
	std::ofstream d;				// zentrales File für debugging
	std::string Input_filename;   	// Für Inputdatenfile
	//Konstruktor:
	T_cfg(int *check);
	//Destruktor
	~T_cfg();
};
		
 T_cfg::T_cfg(int *check){
	
 #ifdef _debug
 std::string filename = "debug.txt"; // Wird im Moment nicht genutzt
 d.open(filename);
 #endif	
	
 #ifdef _logf
    std::string filename1 = "logfile.txt";
	logf.open(filename1);
  	logf << "T_cfg-Konstruktor\n" << "\tIm Moment wird hier definiert, welche Struktur das Modell hat.\n";
    logf << "\t Perspektivisch Definition über Konfigurationsfile\n";
 #endif

 
	kern_anz 	= 2; 			// Anzahl an Normalverteilungskomponenten im Modell; zum Start so gesetzt
	in 			= 1;			// Im Moment auf 1 gesetzt
	dim 		= 1;			// Dimension des Outputverktors, d.h. der zu modellierenden Variable. 1 bedeutet univariater Fall
	count 		= 1000;			// count Datensätze
	*check		= 1;
	max_step	= 10000; 
	Input_filename = "CS.txt";	// Hier wird der Input-Filename festgelegt

};
T_cfg::~T_cfg(){
	#ifdef _logf
	logf << "T_cfg-Destruktor\n";
	#endif
	};