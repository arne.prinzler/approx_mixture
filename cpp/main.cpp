// main.cpp file

#include "main.h"
#include "cfg.cpp"
#include "zufall.cpp"
#include "data.cpp"
#include "verteil.cpp"
#include "em.cpp" 
#include "mixture.cpp"

int main()
{   //das Programm startet an der Stelle generate_em_environment aus mixture.cpp
	//Erzeugung einer Mischverteilung "mixture"
  
 int check=0;
 T_cfg cfg(&check);						// Konfigurationsobjekt wird erzeugt
 T_mixture mixture(&check, &cfg);		// direkte Variante der Erzeugung eines Mischverteilungsobjekts
 T_data inputdata(&cfg, &check);  		// Nach diesem Schritt sind auch die Daten eingelesen und stehen für die Modellschätzung zur Verfügung
 mixture.generate_em_environment(&cfg, &inputdata, &check); // Die EM-Umgebung wird aufgebaut
 mixture.em_mixture_estimation(&check); // Der EM-Algorithmus wird gestartet und durchgeführt
  
  
return 0;
}