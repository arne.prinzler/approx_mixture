/* Definiert und implementiert Klasse T_zufall */
/* Ein Großteil des Codes geht zurück auf: (C) Copr. 1986-92 Numerical Recipes Software i,&. */

//Definitionsbereich//

class T_zufall {

  long *time_init1 = 0;	
	
public: 
  //Gibt N(0,1)-verteilte ZV zurück
  float gasdev();

  //Gibt N(mu,sigma²)-verteilte ZV zurück. Sígma=Standardabweichung.
  double generate_rndv(double mu, double sigma);

  //Gibt den Wert der Zufallsvariable aus ein gemischten Verteilung zurück
  //Übergeben werden die Standardabweichungen
  //double generate_two_comp_mixture(double alpha, double mu1, double sigma1,double mu2, double sigma2);
  
  //Gibt Zufallswert zw. 0 und 1 (gleichverteilt) zurück.
  float random();

  T_zufall();			//Konstruktor: Initialisierung des Zufallsgenerators
  T_zufall(long *seed);	//Konstruktor: Initialisierung mit festem Wert
};


//Implementierungsbereich//

// Konstruktor: Zufallszahlengenerator initialisieren 
T_zufall::T_zufall() {
#ifdef _logfile_zufall
	std::cout << "\nT_zufall - Konstruktor Init mit Systemzeit";
#endif 
  if (time_init1==0) {
    time_init1 = new long;
    time_t now;			

    // Übergibt Systemzeit
    *time_init1=-time(&now); //Grund für das negative Vorzeichen unklar
	std::cout<<"\nSystemzeit "<< time(&now);
  }
};

/*2. Konstruktor: Zufallszahlengenerator wird mit einem festen seed 
initialisiert, falls seed=0 dann Initialisierung mit Systemzeit*/

T_zufall::T_zufall(long *seed) {
#ifdef _logfile_zufall
	std::cout << "\nT_zufall - Konstruktor Init mit festen seed (..)";
#endif 
	
	if (*seed>=0) {
		time_init1=new long;
		time_t now;			
		// Übergibt Systemzeit
		*time_init1=-time(&now);
		//*time_init1=time(&now);
		
		std::cout<<"\nInitialisierung mit (negativer) Systemzeit "<< -time(&now);
		
	}
  else{
		time_init1=seed; 
		std::cout<<"\nInitialisierung mit Seed "<< *seed;
	}
}


float T_zufall::gasdev(){
#ifdef _logfile_zufall
	std::cout << "\nT_zufall::gasdev()";
#endif 
  long *idum;
  idum=time_init1;
  
	//float ran1(long *idum); //Grund unklar
	static int iset=0;
	static float gset;
	float fac,rsq,v1,v2;

	if  (iset == 0) {
		do {
			v1= (float) 2.0*random()/*1(idum)*/ - (float) 1.0;
			v2= (float) 2.0*random()/*1(idum)*/- (float) 1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=(float) sqrt(-2.0*log(rsq)/rsq);
		gset=v1*fac;
		iset=1;
		return v2*fac;
	} else {
		iset=0;
		return gset;
	}
}

//generiert eine normalverteilte Zufallsvariable mit 
//Parametern mu und sigma (Standardabweichung). 
double T_zufall::generate_rndv(double mu, double sigma) {
#ifdef _logfile_zufall
	std::cout << "\nT_zufall::generate_rndv(2)";
#endif 
return (sigma*gasdev()+mu);
}

// gleichverteilte ZV von 0 bis 1
float T_zufall::random()     //ran1 aus nrc. S.280
{	
#ifdef _logfile_zufall
	std::cout << "\nT_zufall::random()";
#endif 
  long *idum;	
  idum = time_init1;

	int j;
	long k;
	static long iy=0;
	static long iv[NTAB];
	float temp;

	if (*idum <= 0 || !iy) {
		if (-(*idum) < 1) *idum=1;
		else *idum = -(*idum);
		for (j=NTAB+7;j>=0;j--) {
			k=(*idum)/IQ;
			*idum=IA*(*idum-k*IQ)-IR*k;
			if (*idum < 0) *idum += IM;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ;
	*idum=IA*(*idum-k*IQ)-IR*k;
	if (*idum < 0) *idum += IM;
	j=iy/NDIV;
	iy=iv[j];
	iv[j] = *idum;
	if ((temp=(float) AM*iy) > RNMX) return (float) RNMX;
	else return temp;
}
/*
 //War im alten Programmcode so. Wahrscheinlich entbehrlich
#undef IA
#undef IM
#undef AM
#undef IQ
#undef IR
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX
#undef ALF
#undef TOLX */
