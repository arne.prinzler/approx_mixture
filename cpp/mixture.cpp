#include "main.h"


//Definitionsbereich
class T_mixture{
private:
	int kern_anz;
	int dim;
    int anzahl;					// Anzahl an Datensätzen 
	int max_step;				// maximale Schrittzahl des EM-Algorithmus
	int theta_length; 			// Länge des Parametervektors
	double *theta_a;  			// altes Parameterset
	double *theta_n;  			// neues Parameterset
	double *theta_save; 		// Parameterset für Speicherzwecke
	std::ofstream *log_file; 	// lokaler Logfile-Zeiger
	
public: 
	//Konstruktor: Eine Mixture stellt das Mischverteilungsmodell dar
	T_mixture(int *check, T_cfg *cfg);
	//Destruktor
	~T_mixture();
	T_normalverteilung **phi;  //Zeiger auf Array von Normalverteilungen
	void generate_em_environment(T_cfg *cfg, T_data *inputdaten, int *check);
	void generate_theta(int *check);
	void em_mixture_estimation(int *check);
	double em_schritte(int *kon, int *schritt_anz);
	
	T_em *p_em;
	T_data *inputdaten;
};

//Impelemtierungsbereich
//Im Konstruktor werden die Konfigurationsdaten übergeben
T_mixture::T_mixture(int *check, T_cfg *cfg){
 #ifdef _logf
  	log_file = &cfg->logf;
	*log_file << "T_mixture-Konstruktor.\n \tDie Mischverteilung wird aufgebaut\n";
 #endif
   	kern_anz = cfg->kern_anz; 	//Anzahl an Normalverteilungskomponenten im Modell; zum Start so gesetzt
	dim 	 = cfg->dim;		//Dimension des Outputverktors, d.h. der zu modellierenden Variable. 1 bedeutet univariater Fall
	max_step = cfg->max_step;
	anzahl 	 = cfg->count;		// Anzahl an Datensätzen
	*check	 = 1;
};
T_mixture::~T_mixture(){
 #ifdef _logf
  	*log_file << "T_mixture-Destruktor\n";
 #endif

	for(int i=0;i<kern_anz;i++) 
	  delete phi[i];	  
	};	

//Die Funktion erzeugt die Normalverteilungskomponenten einer Mischverteilung. In dieser Form sind die 
//Parameter der Normalverteilung noch nicht belegt, dass erfolgt später in deren Konstruktor.	
void T_mixture::generate_em_environment(T_cfg *cfg,T_data *inputdaten, int *check){
 #ifdef _logf
  	*log_file << "\nT_mixture::generate_em_environment().\n";
	*log_file << "\tDie Mischverteilung wird aus Normalverteilungen erzeugt\n";
 #endif
	if (*check==1){
	phi=new T_normalverteilung*[kern_anz];
	for(int i=0;i<kern_anz;i++){ 
	  phi[i] = new T_normalverteilung();
	  
	  // Hiermit auch grds. logfile für die Klasse zugewiesen. Vielleicht gibt es eine bessere Lösung
	  phi[i]->log_file = log_file; 
	}
  //Hier wird das Objekt für den EM;-Schätzalgorithmus erzeugt 
  p_em = new T_em(cfg, phi, inputdaten, check);
  }
  else {*check=0; std::cout << "Fehler generate_em_environment\n";}
 };
 
void T_mixture::generate_theta(int *check){
//Diese Funktion hat es im Originalprogramm nicht gegeben und dient der Erzeugung der Parametervektoren in 
//der Mixture. ggfs später wieder rückbauen.	
//Zuerst die Mischkoeffizienten, dann die Varianzen, dann die Erwartungswerte.
//pro Kern (lambda/alpha, sigma², mu*dim)  //Die Unterscheidung lambda/alpha sagt mir im Moment nichts. Muss ich erst einmal recherchieren. Kann sein, dass es ignoriert werden kann und zum Schluss nicht mehr benutzut wurde
 #ifdef _logf
  	*log_file << "\nT_mixture::generate_theta()\n";
	*log_file << "\t Der Parametervektor wird erzeugt aber noch nicht initialisiert\n";
 #endif
 if (*check == 1){
  theta_length=kern_anz*(2+dim); 		//1 Mischparameter, 1 Streuparameter und dimension Lageparameter. 
										//Bspl. 1 Kern mit bivariaten Daten hat 4 Parameter: p, Sigma^2 und mu_1 
										//und mu_2. Sigma^2 wurde immer nur als 1 Streuparameter, 
										//niemals als Kovarianzmatrix modelliert.
  theta_a = new double[theta_length]; 	//für alte und schlussendlich finale Parameter
  theta_n = new double[theta_length]; 	//für neue Parameter
  theta_save = new double[theta_length]; //wird benötigt, um gute Ergebnisse zwischenzuspeichern
  }
 else *check = 0;
};

//Achtung, diese Funktion ist gegenüber dem Original erheblich ausgedünnt, z.B. ohne MC-Funktion und Bootstrapping 
void T_mixture::em_mixture_estimation(int *check){
 #ifdef _logf
	*log_file << "\nT_mixture::em_mixture_estimation()";
 #endif 
	double loglike;
	int check_singularity;  //Null, wenn Singularität beobachtet
	int konv;  				//Null, wenn Algorithmus nicht konvergiert
	
	double kuip 		= 0.0, //diese 3 Größen nehmen später Statistiken auf. Sie werden aktuell noch nicht
		   chris1 		= 0.0, //praktisch genutzt
		   chris2 		= 0.0;

	int zaehl_sing 		= 0; //zählt, wie häufig im Schätzprozess eine Singularität (ML wird unendlich) aufgetreten ist
	int zaehl_non_konv 	= 0; //klären
	int schritte 		= 0; // Anzahl der Schritte bis zur Konvergenz
	int kum_schritte 	= 0; //klären
	int best_its 	 	= 0; // speichert, welches die beste Schätziteration war. Klären
	double best_loglike;	// zum Zwischenspeichern des besten Schätzergebnisses. Aktue
		
	// Nur Erzeugung von theta
	if(*check==1) generate_theta(check);
		else {*check=0; std::cout << "Fehler bei generate_theta()";}
	
	long seed = 2193736; //Noch nicht final. Negativer Wert führt zu Nicht-Nutzung der Systemzeit
	p_em->initialize_theta(&seed); 
	
	//Temporär für gewollte, feste Initialisierung
	//p_em->initialize_fix(0.5, 0.25,2.25,0.5,1.5); // für 2 Komponenten
	//p_em->initialize_fix(0.3,0.3, 2.0,1.0,0.5,-1.0,1.0,0.0);//3Komponenten
	#ifdef _logf
	*log_file << "\nTheta bei Initialisierung\n";
	for(int i=0;i<theta_length;i++)
				*log_file <<" Theta["<< i << "] = " << p_em->theta_a[i] << "\n";	
  	#endif
	//Wichtig: hier wir der iterative Algorithmus aufgerufen und gibt nach konvergenz auch die Likelihood zurück 
	loglike = em_schritte(&konv,&schritte);
	
	if(konv==0) 
	  zaehl_non_konv++;
	  
	//check=p_em->keep_best_ml_parameters(its,loglike,&best_loglike,&best_its);//Der Maximierungsvorgang findet in EM-Schritte statt, LogLikelihood wird zurückgegeben
	//log_file->write_to_file("\nDer theta-Vektor nach EM-Algorithmus:");
	//log_file->report_parameters(p_em->theta_length, p_em->theta_a);
	//if(check==0)
	   //zaehl_sing++;
	//em_results->mc_end_werte(best_its,p_em->theta_length, p_em->theta_save,&best_loglike,kuip);
	
 #ifdef _logf
    *log_file << "\nTheta nach Abschluss der Schätzung\n";
	for(int i=0;i<theta_length;i++)
		*log_file <<" Theta["<< i << "] = " << p_em->theta_a[i] << "\n";
	*log_file << "\nDie a-posteriori Wahrscheinlichkeiten\n";
	for(int i=0;i<anzahl;i++){
		for(int j=0;j<kern_anz;j++)
				*log_file <<" a posteriori ["<< i << "][" << j <<"] = \t" << p_em->pi_p[i*kern_anz+j] << "\t";
		*log_file <<"\n";	
	}		 
 #endif 
	std::cout<<"\n"<<zaehl_sing<<" Singularitäten und "<<zaehl_non_konv<<" Nicht-Konvergenzen beobachtet";
	//std::cout<<"\nDurchschnittlich "<<kum_schritte/p_cfg->ml_runs<<" EM-Schritte";
	
}

//Führt EM-Algorithmus solange durch bis Abweichung der loglikelihood minimal ist.
double T_mixture::em_schritte(int *kon, int *schritt_anz) {
#ifdef _logf
	*log_file << "\nT_mixture::em_schritte(2)";
#endif 

	double neu_log = -1.0e+10, alt_log = 0;
	*kon = 0;
	//Anzahl der Iterationsschritte
	std::cout << "\nMaximal "<<max_step<<" Schritte";
	//der EM-Algorithmus startet
	int l = 0;
	while (l<max_step) {
		l++;
		p_em->calculate_aposteriori();
		p_em->maximization_step();
		p_em->keep_parameters();
		
		//Für die Schleife
		alt_log = neu_log;
		neu_log = p_em->loglikelihood();
		if (my_abs(alt_log, neu_log)<1.0e-5) {
			std::cout << "\nKonvergenz nach " << l << " Schritten\n";
			*schritt_anz = l;
			l = max_step;
			*kon = 1;
		}
			//em_results->ausgabe(neu_log, p_em->theta_length, p_em->theta_a);
			
	}
	theta_a =  p_em->theta_a; // Übergabe des Parametervektors von p_em an das Mixture_Objekt

	if (*kon == 0)
		std::cout << "\nKeine Konvergenz";
	return neu_log;
}