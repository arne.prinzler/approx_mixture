// em.cpp file
// Definitionsbereich

class T_em {
	private: int a;
	int		anzahl;				// Größe des Datensamples
	int		dimension;			// Dimension des Datenvektors, im Moment 1 gesetzt*/
	int		kern_anz;			// Anzahl an Komponenten des Mischmodells
	double	*datensatz;			// Zur Schätzung verwendeter Datensatz, im Moment univariat
	double	error;				// Fehlergröße zu Kontrollzwecken 
	std::ofstream *log_file; 	// lokaler Logfile-Zeiger
	
	
	
	public:
	T_em(int *check);
	T_em(T_cfg *cfg, T_normalverteilung **komp_den, T_data *daten, int *check);
	double	*pi_p;				// Für die a posteriori-Wahrscheinlichkeiten 
	void initialize_fix(double p, double var1, double var2,double mu1, double mu2);
	void initialize_fix(double p1,double p2,  double var1, double var2,double var3,double mu1, double mu2,double mu3);
	void initialize_theta(long *p_seed);
	void calculate_aposteriori();
	void maximization_step();
	void keep_parameters();
	double loglikelihood();
    double	*theta_a, *theta_n, *theta_save;
	int		theta_length;		// Länge des Parametervektors
	T_normalverteilung **kern;
    int ok;
  	~T_em();
	
};

// Implementationsbereich

//Es werden folgende Vektoren erzeugt: theta_a/n, pi_p;
//Es wird "anzahl"(der Muster), "dimension", "kern_anz" und
//der "datensatz" als Zeiger auf einen Vektor übergeben.
//Desweiteren werden kern_anz-T_normalverteilungsobjekte erzeugt.
//Die jeweils auf den theta_a Vektor zeigen.

T_em::T_em(T_cfg *cfg, T_normalverteilung **komp_den, T_data *daten, int *check) {
  #ifdef _logf
  	log_file = &cfg->logf;
	*log_file << "T_EM-Konstruktor.\n \t Die Komponentendichten und die zu modellierenden Daten werden übergeben\n";
  #endif
  
  anzahl 		= cfg->count;
  dimension 	= cfg->dim;
  kern_anz 		= cfg->kern_anz;
  datensatz		= daten->o_matrix; //im Moment wird nur diese gebraucht
  
  // Zuerst die Mischkoeffizienten, dann die Varianzen,
  // dann die Erwartungswerte.
  
  //pro Kern (lambda/alpha, sigma², mu*dim)
  theta_length=kern_anz*(2+dimension); 

  theta_a = new double[theta_length];
  theta_n = new double[theta_length];
  theta_save = new double[theta_length];

  // aposteriori-Wahrscheinlichkeiten jedes Kerns zu einem Sample, folgen hintereinander
  pi_p = new double[kern_anz*anzahl];  

    kern=komp_den;//Damit für alle Funktionen bekannt
		for(int i=0;i<kern_anz;i++)
   	kern[i]->uebergeben(dimension, i, kern_anz, theta_a);
  
  ok=1;
  *check=ok;
}

void T_em::initialize_fix(double p, double var1, double var2,double mu1, double mu2){

  #ifdef _logf
	*log_file << "\nT_em::initialize_fix für 2 Komponenten";
  #endif
  
  if(kern_anz!=2)
		std::cout<<"\nFalsche Kernanzahl!",ok=0;
  else{
   theta_a[0]=p;
   theta_a[1]=1.0-p;
   theta_a[2]=var1;
   theta_a[3]=var2;
   theta_a[4]=mu1;
   theta_a[5]=mu2;
  }
	
}

void T_em::initialize_fix(double p1,double p2,  double var1, double var2,double var3,double mu1, double mu2,double mu3){
#ifdef _logf
	*log_file << "\nT_em::initialize_fix für 3 Komponenten";
#endif 

  if(kern_anz!=3)
		std::cout<<"\nFalsche Kernanzahl!",ok=0;
  else{
    theta_a[0]=p1;
	theta_a[1]=p2;
    theta_a[2]=1.0-p1-p2;
	theta_a[3]=var1;
	theta_a[4]=var2;
	theta_a[5]=var3;
	theta_a[6]=mu1;
	theta_a[7]=mu2;
	theta_a[8]=mu3;
  }
	
}

// Startwerte für die Mischparameter werden zufällig gesetzt
// theta[2*kern_anz+i*dimension+j] bezeichnet die j-te Komponente
// des Erwartungswertvektors des i-ten kerns
// Erwartungswerte können auch negativ sein 
// Funktion hieß früher T_em::generate_theta(long *p_seed). Ist so aber klarer, worum es geht

void T_em::initialize_theta(long *p_seed){
#ifdef _logf
	*log_file << "\nT_em::initialize_theta(..)\n";
	*log_file << "\tHier wird der Parameterverktor zufallsinitialisiert";
#endif 
   
  //write_to_debug_file("\nT_em::generate_theta");
  // Zufallsgenerator
  T_zufall *rand;
  rand = new T_zufall(p_seed);
  //cout<<"\nErste Zufallszahl "<<rand->random();
  int i;
  double sum=0.0;

  // Jeder Parameter des Vektors wird mit einem Zufallswert 
  // gleichverteilt von -1 und +1 belegt.
  for(i=0;i<theta_length;i++)
 	theta_a[i]=((double)(rand->random()*2.0))-1.0; 
  
  //Zwischenschritt für Mischkoeffizienten 
  for(i=0;i<kern_anz;i++) {
	theta_a[i]=exp(theta_a[i]);
	sum+=theta_a[i];  
  }
  
  //Normierung der Mischkoeffizienten und Initialisieren der  Varianzen
  for(i=0;i<kern_anz;i++) {
	theta_a[i]/=sum;					// Mischkoeffizienten
	theta_a[kern_anz+i]=exp(theta_a[kern_anz+i]); //Varianzen
  }
  
  //write_to_debug_file("\nDas erste theta_a: ");
  //debug_report_theta_a();

  delete rand;
  
}



//	Berechnung der a posteriori Wahrscheinlichkeiten, 
//	pi_p[j+i] enthält die Wahrscheinlichkeit, daß der j-te
//	Beobachtungswert aus der i-ten Mischkomponente stammt.

//  Hinweis:
//  an die Funktion kernfct wird datensatz+j*dimension übergeben.
//  Das ist die Adresse des 1. Datenpunkts des j-ten Vektors der Beobachtungswerte

inline double my_abs(double a){
if(a>=0.0)
	return (a);
else return (-a);
}
//stand ursprünglich in desc_nrc.cpp
inline double my_abs(double a,double b){
if((a-b)>=0.0)
	return (a-b);
else return (b-a);
}

void T_em::calculate_aposteriori() {
#ifdef _logf
	*log_file << "\nT_em::calculate_aposteriori()";
#endif

  //write_to_debug_file("\ncalculate a posteriori");

  int i,j;
  double sum=0.0;

  // für jedes Muster
  for(j=0;j<anzahl;j++) { 
	// für jeden Kern
	for(i=0;i<kern_anz;i++)	{
	  // f(y_j|i) Likelihood der i-ten Komponente 
	  pi_p[j*kern_anz+i]=kern[i]->gauss(datensatz+j*dimension); 
	  // f(y_j) Likelihood der Mischung 
	  sum+=theta_a[i]*pi_p[j*kern_anz+i];			
	}
	for(i=0;i<kern_anz;i++) {
	  //pi_p[i][j]; Hier liegt die Posteriori Wahrscheinlichkeit vor
	  pi_p[j*kern_anz+i]*=(theta_a[i]/sum); 
	}
	sum=0.0;	
  }
  //debug_report_a_posteriori();
}

void T_em::maximization_step() {
#ifdef _logf
	*log_file << "\nT_em::maximization_step() - Bayes-Version eliminiert";
#endif

  //write_to_debug_file("\nmaximization_step");
  int i,j,k;
  
  double sum;
  double skalarprod;

  //Berechnung der neuen Mischkoeffizienten
  //keine Versionsunterscheidung notwendig
  for(i=0;i<kern_anz;i++) {
	sum=0.0;
	for(j=0;j<anzahl;j++)
	  sum+=pi_p[j*kern_anz+i];
	theta_n[i]=sum/anzahl;
  }	
  
  //Berechnung der neuen Lagekoeffizienten
  for(i=0;i<kern_anz;i++) {
	for(k=0;k<dimension;k++){
	  sum=0.0;
		for(j=0;j<anzahl;j++)	{                       
			sum+=pi_p[j*kern_anz+i]*datensatz[j*dimension+k];
		//file<<"x["<<j<<"]["<<k<<"] "<<datensatz[j*dimension+k];
		}
	  
		theta_n[2*kern_anz+i*dimension+k]=sum/(anzahl*theta_n[i]);
	} 
  } 
  
  double sum_bayes;
  //Berechnung der neuen Streukoeffizienten
  for(i=0;i<kern_anz;i++) {
	sum=0.0;
	sum_bayes=0.0;
	for(j=0;j<anzahl;j++) {
	  skalarprod=0.0;
	  for(k=0;k<dimension;k++)
		skalarprod+=pow(datensatz[j*dimension+k]-theta_n[2*kern_anz+i*dimension+k],2);
	  sum+=pi_p[j*kern_anz+i]*skalarprod;
	  sum_bayes+=pi_p[j*kern_anz+i];
	  
	}
	theta_n[kern_anz+i]=sum/(dimension*anzahl*theta_n[i]);
	
  }	
}

//	Die neu berechneten Parameter stehen als alte Parameter im nächsten
//	Maximization Step wieder zur Verfügung
void T_em::keep_parameters() {
#ifdef _logf
	*log_file << "\nT_em::keep_parameters()";
#endif 

  //write_to_debug_file("\nkeep_parameters");
  int i;
  for(i=0;i<theta_length;i++)
	theta_a[i]=theta_n[i];
  //debug_report_theta_a();
}


// Siehe z.B. Bishop S 67 unbedingt überprüfen
// Berechnet die Loglikelihood auf Grundlage von theta_a
double T_em::loglikelihood() {
#ifdef _logf
	*log_file << "\nT_em::loglikelihood()";
#endif 

  //write_to_debug_file("\nT_em::loglikelihood");
  double sum;
  double logl = 0.0;
  int 	check = 1;
  //Varianztest
  for (int j=0; j<kern_anz;j++){
	  if(my_abs(*kern[j]->sigma_q,0.0)<1.0e-5)
		  check*=0;
  }
  if(check==1){
   for (int i=0; i<anzahl;i++) {
	sum=0.0;
	for (int j=0;j<kern_anz;j++) {
	  sum+=theta_a[j]*kern[j]->gauss(datensatz+i*dimension);
	}
	logl+=log(sum);
   }
   return logl;
  }
  else return 1.0e+7;
}


//Destruktor
T_em::~T_em() {
#ifdef _logf
  	*log_file << "T_EM-Destruktor.\n";
  #endif
}

