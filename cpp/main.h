//gedacht für globale variablen, die Programmweit zur Verfügung stehen. 
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
 
 //globale log- und debugfiles
 #define _logf 	// Wenn eingeschaltet, dann wird ein logfile geöffnet und der Programmablauf ausgegeben
 #define _debug // analog, aber in Bezug auf (Zwischen)ergebnisse
 #define pi 3.14159265358979
 #define PI pi
 
 // Für Zufallszahlenerzeugung in T_zufall
 #define IA 16807
 #define IM 2147483647
 #define AM (1.0/IM)
 #define IQ 127773
 #define IR 2836
 #define NTAB 32
 #define NDIV (1+(IM-1)/NTAB)
 #define EPS 1.2e-7
 #define RNMX (1.0-EPS)
 #define ALF 1.0e-4
 #define TOLX 1.0e-7

 
