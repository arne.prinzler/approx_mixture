/** Klasse zur Verwaltung sämtlicher Datensätze. Dazu gehören Trainings-, 
Validierungs-  und Testdatensätze, sowie Portfolios*/
/* **************************************************************/

class T_data {	
private:
	double *h; 					// Hilfsvektor
	std::ofstream *log_file; 	// lokaler Logfile-Zeiger
	
public: 

  int ok; 			// Kontrollvariable
  int count;		// Anzahl der an Datensätzen (Pattern)		 
  double *i_matrix; // Vektor der Inputdaten	
  double *o_matrix; // Vektor der Output-Trainingsdaten
  int in; 			// Anzahl der Inputneuronen
  int out; 			// Anzahl der Outputneuronen

/** Erster Konstruktor: Wichtig für das Einlesen der Datensätze aus dem Datenfile*/
//T_data(char *data_f,char *portfolio_f,const int &i_count,const int &o_count,const  int &boot,int &aufgabe);	 
// Hilfskonstruktor
  T_data(T_cfg *cfg, int *check);


void datenausgabe(char *string);

/**  Destruktor */  
  ~T_data();     
};
           				 

 T_data::T_data(T_cfg *cfg, int *check)
 {
  #ifdef _logf
  	log_file = &cfg->logf;
	*log_file << "T_data-Konstruktor.\n \tDas Datenobjekt wird aufgebaut. Hier auch noch Festlegung des Inputfiles\n";
  #endif
  
  in 	= cfg->in; 		// Wird nur aus technischen Gründen verwendet, um das Programm möglichst Original halten zu können.
  out 	= cfg->dim; 	// Dimension der zu modellierenden Daten
  count = cfg->count; 	// Anzahl an Daten
  
  if (*check ==1){
  std::ifstream daten(cfg->Input_filename);
  char str[100] = "";
	daten >> str >> str; // Achtung: das ist erst einmal sehr hemdsärmelig gebaut und dient dazu die 
						 // Spaltenbeschriftung x und y einzulesen. Muss später verbessert werden.

  i_matrix = new double[count*in];  	// Array erzeugen mit Patternanzahl "count" und Anzahl derbedingenden Größe x "in". 
  o_matrix = new double[count*out];		// dito für out für die zu modellierende Größe y
  int i, k;								// Zählgrößen
 
 //Einlesen der Daten
  for (i = 0; i<count; i++) {
	for (k = 0; k<in;  k++) daten >> i_matrix[i*in  + k];
	for (k = 0; k<out; k++) daten >> o_matrix[i*out + k];
   }
   //Ausgabe zu Testzwecken
   /*for (i = 0; i<count; i++) {
		for (k = 0; k<in;  k++) std::cout << "i_matrix " << i_matrix[i*in  + k] <<"\n";
		for (k = 0; k<out; k++) std::cout << "o_matrix " << o_matrix[i*out + k] <<"\n";
	} */
  }
 else {*check=0; std::cout << "Fehler T_data Konstruktor\n";}
 };

 
 T_data::~T_data(){
 #ifdef _logf
  	*log_file << "T_Data-Destruktor.\n";
 #endif
	};