//Definitionsbereich 
class T_normalverteilung  {
private:
  int k;			  // k: Dimension des Datenvektors
  int kern_anz;
  double init;
    
public:
  double perz;		  		// Perzentil, nur für eindim. Gauss-Kernfunktion, wird bei VaR-Berechnung benötigt 
  double value;				// Variable für den Wert der Dichtefunktion einer Normalverteilung
  double *mischkoeff;   	// noch aus dem alten Programm übernommen. Kann ggfs. später weg.
  double *mu;				// mu-Vektor des Kerns
  //double **mu_adr;		// Bedeutung unklar
  double *sigma_q;      	// Sigma² des Kerns
  double *sigma;        	// Sigma des Kerns
  
  std::ofstream *log_file; 	// lokaler Logfile-Zeiger
  T_normalverteilung();     // Konstruktor 	
 ~T_normalverteilung();     // Konstruktor 	
  void uebergeben(int dim, int kern_nr, int kerne, double *theta);
  double gauss(double *x);
  
};

//Implementierungsbereich 
T_normalverteilung::T_normalverteilung() {
 
	value		= 0.0;
	init		= 0.0;
	mischkoeff	= &init;	//noch aus dem alten Programm übernommen. Kann ggfs. später weg.
	sigma		= &init;	//noch aus dem alten Programm übernommen. Kann ggfs. später weg.
	perz		= 0.0;
	mu			= &init;
}
T_normalverteilung::~T_normalverteilung() {
*log_file << "T_normalverteilung - Destruktor\n";
	}
	
// übergibt die Dimension, sigma und den mu-Vektor
void T_normalverteilung::uebergeben(int dim, int kern_nr, int kerne, double *theta) {
  k 		 = dim;						  	// Dimension wird übergeben.
  mischkoeff = theta+kern_nr;
  sigma_q 	 = theta+kerne+kern_nr;	  		// Position von sigma² wird uebergeben.
  mu 		 = theta+2*kerne+dim*kern_nr;	// erste Position des mu-Vektors in theta wird übergeben.
}

//Gibt den Dichtewert der Normalverteilung 
//eines Kerns zu den Daten des Vektor *x zurück.
double T_normalverteilung::gauss(double *x) {
#ifdef _logf_verteil
	*log_file << "\nT_normalverteilung::gauss(..)"; 
#endif

  int j; // fuer die j-te Dimension 
  double skalarprodukt=0.0; 
 
  // für jede Dimension
  for(j=0;j<k;j++) 
	skalarprodukt+=(x[j]-mu[j])*(x[j]-mu[j]);
  value=pow(2*pi,-0.5*k)*pow(*sigma_q,-0.5*k)*exp(-skalarprodukt/(2*(*sigma_q))); 

  return (value);
}
